import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "@/views/Home";

//Fix
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/reports',
        name: 'Reports',
        component: () => import('../views/Reports/List.vue'),
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Auth/Login.vue'),
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('../views/Auth/Register.vue'),
    },
    {
        path: '/post/:id',
        name: 'Post',
        component: () => import('../views/Post/View.vue'),
    },
    {
        path: '*',
        name: '404',
        component: () => import('../views/Error/404.vue'),
    }

]

const router = new VueRouter({
    mode: "history",
    routes: routes,
    linkExactActiveClass: "active",
    linkActiveClass: null,
})

export default router
