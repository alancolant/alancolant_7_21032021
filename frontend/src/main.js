import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import lsWatcher from "vue-storage-watcher"
import VueProgressBar from 'vue-progressbar'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueMeta from 'vue-meta'

Vue.config.productionTip = false
Vue.use(VueToast);
Vue.use(VueMeta)
Vue.use(VueProgressBar, {
    color: '#4f46e5',
    failedColor: '#cd0101',
    thickness: '2px',
    transition: {
        speed: '1.0s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
})
Vue.use(lsWatcher, {prefix: "mp_"})
Vue.prototype.$http = axios.create({
    baseURL: 'http://192.168.122.1:3000/api',
})

Vue.mixin({
    filters: {
        capitalize: value => {
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
        toHumanDate: value => {
            return new Date(value).toLocaleDateString(undefined, {year: 'numeric', month: 'long', day: 'numeric'})
        },
        truncate: (value, stop) => {
            return value.slice(0, stop) + (stop < value.length ? '...' : '')
        }
    },
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')


