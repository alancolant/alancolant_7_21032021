# Guide de démarrage

### Avec docker-composer(simulation production)

##### Pour démarrer le service

```sh
docker-compose up -d front_prod back_prod # |front_dev back_dev| # or docker-compose up dev|prod to run in non-detached mode
```

##### Pour quitter le service

```sh
docker-compose down # or ctrl+c if is running in non-detached mode
```

____

### Sans docker-compose

##### Frontend

```sh
cd frontend
yarn # or npm i 
yarn serve # or npm run serve
```

##### Backend

```sh
cd backend
yarn # or npm i 
yarn serve # or npm run serve
```

____

# Guide d'utilisation

#### L'application démarre [sur le port 8080](http://127.0.0.1:8080)

#### L'accès à l'API [sur le port 3000/api](http://127.0.0.1:3000/api)

____

## Prérequis (uniquement si utilisation hors docker-compose)

- NodeJS (développé en v14.16.0 LTS)
- yarn ou npm

____

## Production

Pour compiler de manière statique le front-end

```sh
cd frontend
yarn build # or npm run build
```

Utilisez apache ou nginx pointant sur le dossier `dist` avec mod_rewrite activé (apache2) ou des règles de redirection(
nginx)

____

# Authentification

## Admin

    email: admin@groupomania.com
    password: azerty

## Employee

    email: employee@groupomania.com
    password: azerty