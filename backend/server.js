const express = require('express')
const app = express()
const config = require('./config/global.json');


//Initialisation du routeur sur /api
const router = require('./routes/router');


//Route d'accès aux photos
app.use('/images/', express.static(config.imagePath));

//Routes API
app.use('/api/', router);

//On démarre le serveur
app.listen(config.port, () => {
    console.log(`BackEnd démarré sur http://localhost:${config.port}`)
})
