const User = require("../../app/Models").User;
const jwt = require('jsonwebtoken');
const config = require('../../config/global.json');

module.exports = (request, res, next) => {
    try {
        const token = request.header('Authorization').replace('Bearer ', '');
        const decodedToken = jwt.verify(token, config.randomKey);
        User.findByPk(decodedToken.id).then(user => {
            request.authUser = user
            next();
        }).catch(err => {
            throw err
        });
    } catch (err) {
        return res.status(401).json({error: 'Authentification problem!'})
    }
};