const AuthController = require("../app/Controllers/AuthController");
const UserController = require("../app/Controllers/UserController");
const PostController = require("../app/Controllers/PostController");
const CommentController = require("../app/Controllers/CommentController");
const ReportController = require("../app/Controllers/ReportController");

const express = require('express');
const router = express.Router();
const cors = require('./middleware/corsMiddleware')

const auth = require('./middleware/authMiddleware')
const formidable = require('express-formidable');
const throttle = require("express-throttle"); //Protection contre spam

//Routes relatives aux middleware(cors et parsage des requêtes)
router.use(cors);
router.use(formidable());


//Routes relatives à l'authentification
router.post('/auth/login', throttle({"rate": "2/s"}), AuthController.login);
router.post('/auth/signup', throttle({"rate": "1/s"}), AuthController.signup);
router.get('/auth/check', auth, function (req, res, _) {
    return res.json('OK');
});

//Routes ressources user
router.get('/user', throttle({"rate": "1/s"}), auth, UserController.index);
router.post('/user', throttle({"rate": "1/s"}), auth, UserController.store);
router.get('/user/:id', throttle({"rate": "1/s"}), auth, UserController.show);
router.put('/user/:id', throttle({"rate": "1/s"}), auth, UserController.update);
router.delete('/user/:id', throttle({"rate": "1/s"}), auth, UserController.destroy);

//Routes ressources post
router.get('/post', throttle({"rate": "1/s"}), auth, PostController.index);
router.post('/post', throttle({"rate": "1/s"}), auth, PostController.store);
router.get('/post/:id', throttle({"rate": "1/s"}), auth, PostController.show);
router.put('/post/:id', throttle({"rate": "1/s"}), auth, PostController.update);
router.delete('/post/:id', throttle({"rate": "1/s"}), auth, PostController.destroy);

//Routes ressources comment
router.get('/comment', throttle({"rate": "1/s"}), auth, CommentController.index);
router.post('/comment', throttle({"rate": "1/s"}), auth, CommentController.store);
router.get('/comment/:id', throttle({"rate": "1/s"}), auth, CommentController.show);
router.put('/comment/:id', throttle({"rate": "1/s"}), auth, CommentController.update);
router.delete('/comment/:id', throttle({"rate": "1/s"}), auth, CommentController.destroy);

//Routes ressources report
router.get('/report', throttle({"rate": "1/s"}), auth, ReportController.index);
router.post('/report', throttle({"rate": "1/s"}), auth, ReportController.store);
router.get('/report/:id', throttle({"rate": "1/s"}), auth, ReportController.show);
router.put('/report/:id', throttle({"rate": "1/s"}), auth, ReportController.update);
router.delete('/report/:id', throttle({"rate": "1/s"}), auth, ReportController.destroy);
module.exports = router;