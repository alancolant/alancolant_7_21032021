'use strict';
const bcrypt = require("bcrypt");
const {Model} = require('sequelize');
const CryptoJS = require("crypto-js");
const config = require('../../config/global.json')


function encrypt(value) {
    return CryptoJS.AES.encrypt(value, CryptoJS.enc.Utf8.parse(config.randomKey), {
        iv: {
            words: [0, 0, 0, 0],
            sigBytes: 16,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }
    }).toString()
}

function decrypt(value) {
    const decrypted = CryptoJS.AES.decrypt(value, CryptoJS.enc.Utf8.parse(config.randomKey), {
        iv: {
            words: [0, 0, 0, 0],
            sigBytes: 16,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }
    });
    return decrypted.toString(CryptoJS.enc.Utf8)
}

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate(models) {
            // define association here
        }

        toJSON() {
            this.setDataValue('email', decrypt(this.getDataValue('email')))
            let attr = this.dataValues;
            // attr.email = decrypt(attr.email)
            delete attr.password;
            // attr.email = decrypt(attr.email)
            return attr;
        }
    }

    User.init({
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'employee'
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            get() {
                const value = this.getDataValue('email');
                if (value) {
                    return decrypt(value);
                } else {
                    return null;
                }
            },
            set(value) {
                if (value) {
                    this.setDataValue('email', encrypt(value));
                } else {
                    this.setDataValue('email', null);
                }
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,

            // Automatic hash password
            set(value) {
                const hash = bcrypt.hashSync(value, 12);
                this.setDataValue('password', hash);
            },
        },
    }, {
        sequelize,
        modelName: 'User',
        tableName: 'users',
    });
    return User;
};