const config = require("../../config/global.json")

'use strict';
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Post extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
            this.hasMany(models.Comment, {foreignKey: 'postId', as: 'comments'});
            this.hasOne(models.Document, {foreignKey: 'postId', as: 'document'});
        }

        toJSON() {
            let attributes = Object.assign({}, this.get());
            if (attributes.document) {
                attributes.documentUrl = new URL('/images/' + attributes.document.imagePath, config.baseUrl)
            }
            return attributes;
        }
    }

    Post.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        moderated: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        userId: {
            type: DataTypes.INTEGER,
        }
    }, {
        sequelize,
        modelName: 'Post',
        tableName: 'posts',
    });
    return Post;
};