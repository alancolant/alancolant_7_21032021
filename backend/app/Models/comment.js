'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Comment extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.Post, {foreignKey: 'postId', as: 'post'});
            this.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
        }
    }

    Comment.init({
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        postId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        parentId: {
            type: DataTypes.INTEGER,
        },
        message: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        moderated: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, {
        sequelize,
        tableName: 'comments',
        modelName: 'Comment',
    });
    return Comment;
};