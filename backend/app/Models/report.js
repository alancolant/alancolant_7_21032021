'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Report extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.User, {foreignKey: 'userId', as: 'user'});
            this.belongsTo(models.Comment, {foreignKey: 'commentId', as: 'comment'});
            this.belongsTo(models.Post, {foreignKey: 'postId', as: 'post'});

        }
    }

    Report.init({
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        commentId: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        postId: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        verified: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        message: DataTypes.TEXT
    }, {
        sequelize,
        tableName: 'reports',
        modelName: 'Report',
    });
    return Report;
};