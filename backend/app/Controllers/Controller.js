const {Model} = require('sequelize');

class Controller {
    /**
     * @param {string} method Method to test
     * @param {string} type
     * @param {User} auth Authenticated User
     * @param {Model} model Model to search
     * @return boolean
     */
    authorize(method, type, auth, model = null) {
        const policy = require('../Policies/' + type + 'Policy')
        if (typeof policy[method] !== "function") {
            return false
        }
        if (model !== null) {
            return policy[method](auth, model)
        } else {
            return policy[method](auth)
        }
    }
}

module.exports = Controller

