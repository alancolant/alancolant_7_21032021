const Post = require("../Models").Post
const User = require("../Models").User
const Comment = require("../Models").Comment
const Document = require("../Models").Document
const config = require("../../config/global.json")
const Controller = require("./Controller");
const fs = require("fs");
const uuid = require("uuid");


class PostController extends Controller {
    index(req, res, _) {
        if (super.authorize('list', 'Post', req.authUser)) {
            return Post.findAndCountAll({
                order: [['id', 'DESC']],
                include: [
                    {
                        model: User,
                        as: 'user',
                        attributes: {exclude: ['password']},
                    },
                    {
                        model: Document,
                        as: 'document',
                    },
                    {
                        model: Comment,
                        as: 'comments',
                        include: [
                            {
                                model: User,
                                as: 'user',
                                attributes: {exclude: ['password']},
                            },
                        ]
                    }],
            }).then(posts => res.json({
                status: 'ok',
                code: 200,
                data: posts
            })).catch(_ => {
                res.status(500).json({
                    status: 'error',
                    code: 500,
                    message: 'Server error.'
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t list resource.',
                code: 401,
            });
        }
    }

    show(req, res, _) {
        Post.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: {exclude: ['password']},
                },
                {
                    model: Document,
                    as: 'document',
                },
                {
                    model: Comment,
                    as: 'comments',
                    include: [
                        {
                            model: User,
                            as: 'user',
                            attributes: {exclude: ['password']},
                        },
                    ]
                }],
        }).then(post => {
            if (!post) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Post Not Found."
                });
            } else {
                if (super.authorize('show', 'Post', req.authUser, post)) {
                    return res.status(200).json({status: 'ok', code: 200, data: post.toJSON()});
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't view this resource."
                    });
                }
            }
        }).catch(_ => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    store(req, res, _) {
        if (super.authorize('create', 'Post', req.authUser)) {
            //Insertion du userId
            req.fields['userId'] = req.authUser.id;
            return Post.create(req.fields).then(post => {
                if (req.files.image !== undefined) {
                    //On récupère l'extension de l'image et on génère un nouveau nom de fichier
                    const extension = req.files.image.name.split('.').pop()
                    const newName = uuid.v4() + '.' + extension;
                    const dir = __dirname + '/../../' + config.imagePath + '/' + post.id;
                    //On déplace le fichier temporaire
                    if (!fs.existsSync(dir)) {
                        fs.mkdirSync(dir);
                    }
                    //On copie le fichier temporaire a l'emplacement souhaité
                    fs.copyFile(req.files.image.path, dir + '/' + newName, err => {
                        if (err) throw err;
                        fs.unlink(req.files.image.path, err => {
                            if (err) throw err;
                            //On crée une entrée dans la table documents
                            Document.create({
                                userId: req.authUser.id,
                                postId: post.id,
                                imagePath: post.id + '/' + newName,
                            }).then(_ => {
                                return res.json({
                                    status: 'ok',
                                    code: 200,
                                    data: post.toJSON()
                                })
                            });
                        });
                    });
                } else {
                    return res.json({
                        status: 'ok',
                        code: 200,
                        data: post.toJSON()
                    })
                }
            }).catch(err => {
                const formatted_errors = {};
                err.errors.map(e => {
                    formatted_errors[e.path] = e.message;
                })
                return res.status(400).json({
                    status: 'error',
                    code: 400,
                    errors: formatted_errors
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t create resource.',
                code: 401,
            });
        }
    }

    update(req, res, _) {
        Post.findByPk(req.params.id).then(post => {
            if (!post) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Post Not Found."
                });
            } else {
                if (super.authorize('edit', 'Post', req.authUser, post)) {
                    post.update(req.fields).then(post => {
                        if (req.files.image !== undefined) {
                            //On récupère l'extension de l'image et on génère un nouveau nom de fichier
                            const extension = req.files.image.name.split('.').pop()
                            const newName = uuid.v4() + '.' + extension;
                            const dir = __dirname + '/../../' + config.imagePath + '/' + post.id;

                            //Suppression 'radicale' du dossier d'image du post (évite un call sql en plus)
                            if (fs.existsSync(dir)) {
                                fs.rmdirSync(dir, {recursive: true});
                            }
                            fs.mkdirSync(dir);

                            //On copie le fichier temporaire a l'emplacement souhaité
                            fs.copyFile(req.files.image.path, dir + '/' + newName, err => {
                                if (err) throw err;
                                fs.unlink(req.files.image.path, err => {
                                    if (err) throw err;
                                    //On met à jour la table documents
                                    Document.findOne({where: {postId: post.id}}).then(document => {
                                        //Création si inexistant
                                        if (!document) {
                                            Document.create({
                                                userId: post.userId,
                                                postId: post.id,
                                                imagePath: post.id + '/' + newName,
                                            }).then(document => {
                                                post.document = document;
                                                return res.json({
                                                    status: 'ok',
                                                    code: 200,
                                                    data: post.toJSON()
                                                })
                                            });
                                        }
                                        //Modification si déjà existant
                                        else {
                                            Document.update({
                                                imagePath: post.id + '/' + newName,
                                            }, {
                                                where: {
                                                    postId: post.id,
                                                }
                                            }).then(document => {
                                                post.document = document;
                                                return res.json({
                                                    status: 'ok',
                                                    code: 200,
                                                    data: post.toJSON()
                                                })
                                            });
                                        }
                                    })
                                });
                            });
                        } else {
                            return res.status(200).json({status: 'ok', code: 200, data: post.toJSON()});
                        }
                    }).catch(err => {
                        const formatted_errors = {};
                        err.errors.map(e => {
                            formatted_errors[e.path] = e.message;
                        })
                        return res.status(400).json({
                            status: 'error',
                            code: 400,
                            errors: formatted_errors
                        });
                    });
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't update this resource."
                    });
                }
            }
        }).catch(err => {
            console.log(err)
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    destroy(req, res, _) {
        Post.findByPk(req.params.id).then(post => {
            if (!post) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Post Not Found."
                });
            } else {
                if (super.authorize('delete', 'Post', req.authUser, post)) {
                    post.destroy().then(_ => {
                        const dir = __dirname + '/../../' + config.imagePath + '/' + post.id;
                        //On déplace le fichier temporaire
                        if (fs.existsSync(dir)) {
                            fs.rmdirSync(dir, {recursive: true});
                        }
                        Document.destroy({
                            where: {
                                postId: post.id,
                            }
                        }).then(_ => {
                            return res.status(200).json({status: 'ok', code: 200, message: 'Post deleted.'});
                        })
                    })
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't delete this resource."
                    });
                }
            }
        }).catch(_ => {
            return res.status(500).json({status: 'error', code: 500, message: 'Server error.'});
        });
    }
}


module.exports = new PostController();