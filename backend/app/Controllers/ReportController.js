const User = require("../Models").User
const Report = require("../Models").Report
const Comment = require("../Models").Comment

const Controller = require("./Controller");

class ReportController extends Controller {
    index(req, res, _) {
        if (super.authorize('list', 'Report', req.authUser)) {
            return Report.findAndCountAll({
                order: [['createdAt', 'DESC']],
                include: [
                    {
                        model: User,
                        as: 'user',
                        attributes: {exclude: ['password']},
                    },
                    {
                        model: Comment,
                        as: 'comment',
                    }
                ],
            }).then(reports => res.json({
                status: 'ok',
                code: 200,
                data: reports
            })).catch(_ => {
                res.status(500).json({
                    status: 'error',
                    code: 500,
                    message: 'Server error.'
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t list resource.',
                code: 401,
            });
        }
    }

    show(req, res, _) {
        Report.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: {exclude: ['password']},
                }
            ]
        }).then(report => {
            if (!report) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Report Not Found."
                });
            } else {
                if (super.authorize('show', 'Report', req.authUser, report)) {
                    return res.status(200).json({status: 'ok', code: 200, data: report.toJSON()});
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't view this resource."
                    });
                }
            }
        }).catch(_ => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    store(req, res, _) {
        if (super.authorize('create', 'Report', req.authUser)) {
            //Insertion du userId
            req.fields['userId'] = req.authUser.id;
            return Report.create(req.fields).then(() => {
                    return res.json({
                        status: 'ok',
                        code: 200,
                    })
                }
            ).catch(err => {
                const formatted_errors = {};
                err.errors.map(e => {
                    formatted_errors[e.path] = e.message;
                })
                return res.status(400).json({
                    status: 'error',
                    code: 400,
                    errors: formatted_errors
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t create resource.',
                code: 401,
            });
        }
    }

    update(req, res, _) {
        Report.findByPk(req.params.id).then(report => {
            if (!report) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Report Not Found."
                });
            } else {
                if (super.authorize('edit', 'Report', req.authUser, report)) {
                    report.update(req.fields).then(report => {
                        let datas = report.toJSON()
                        return res.status(200).json({status: 'ok', code: 200, data: datas});
                    }).catch(err => {
                        console.log(err);
                        const formatted_errors = {};
                        err.errors.map(e => {
                            formatted_errors[e.path] = e.message;
                        })
                        return res.status(400).json({
                            status: 'error',
                            code: 400,
                            errors: formatted_errors
                        });
                    });
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't update this resource."
                    });
                }
            }
        }).catch(err => {
            console.log(err)
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    destroy(req, res, _) {
        Report.findByPk(req.params.id).then(report => {
            if (!report) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Report Not Found."
                });
            } else {
                if (super.authorize('delete', 'Report', req.authUser, report)) {
                    report.destroy().then(_ => {
                        return res.status(200).json({status: 'ok', code: 200, message: 'Report deleted.'});
                    }).catch(_ => {
                        return res.status(500).json({status: 'error', code: 500, message: 'Server error.'});
                    })
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't delete this resource."
                    });
                }
            }
        }).catch(_ => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }
}


module.exports = new ReportController();