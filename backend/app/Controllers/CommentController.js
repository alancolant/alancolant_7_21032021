const User = require("../Models").User
const Comment = require("../Models").Comment

const Controller = require("./Controller");

class CommentController extends Controller {
    index(req, res, next) {
        if (super.authorize('list', 'Comment', req.authUser)) {
            return Comment.findAndCountAll({
                order: [['createdAt', 'DESC']],
                include: [
                    {
                        model: User,
                        as: 'user',
                        attributes: {exclude: ['password']},
                    }
                ],
            }).then(comments => res.json({
                status: 'ok',
                code: 200,
                data: comments
            })).catch(err => {
                res.status(500).json({
                    status: 'error',
                    code: 500,
                    message: 'Server error.'
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t list resource.',
                code: 401,
            });
        }
    }

    show(req, res, next) {
        Comment.findByPk(req.params.id, {
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: {exclude: ['password']},
                }
            ]
        }).then(comment => {
            if (!comment) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Comment Not Found."
                });
            } else {
                if (super.authorize('show', 'Comment', req.authUser, comment)) {
                    return res.status(200).json({status: 'ok', code: 200, data: comment.toJSON()});
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't view this resource."
                    });
                }
            }
        }).catch(err => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    store(req, res, next) {
        if (super.authorize('create', 'Comment', req.authUser)) {
            //Insertion du userId
            req.fields['userId'] = req.authUser.id;
            return Comment.create(req.fields).then(comment => {
                    let datas = comment.toJSON();
                    datas.user = req.authUser
                    return res.json({
                        status: 'ok',
                        code: 200,
                        data: datas
                    })
                }
            ).catch(err => {
                const formatted_errors = {};
                err.errors.map(e => {
                    formatted_errors[e.path] = e.message;
                })
                return res.status(400).json({
                    status: 'error',
                    code: 400,
                    errors: formatted_errors
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t create resource.',
                code: 401,
            });
        }
    }

    update(req, res, next) {
        Comment.findByPk(req.params.id).then(comment => {
            if (!comment) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Comment Not Found."
                });
            } else {
                if (super.authorize('edit', 'Comment', req.authUser, comment)) {
                    comment.update(req.fields).then(comment => {
                        let datas = comment.toJSON()
                        datas.user = req.authUser
                        return res.status(200).json({status: 'ok', code: 200, data: datas});
                    }).catch(err => {
                        const formatted_errors = {};
                        err.errors.map(e => {
                            formatted_errors[e.path] = e.message;
                        })
                        return res.status(400).json({
                            status: 'error',
                            code: 400,
                            errors: formatted_errors
                        });
                    });
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't update this resource."
                    });
                }
            }
        }).catch(err => {
            console.log(err)
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    destroy(req, res, next) {
        Comment.findByPk(req.params.id).then(comment => {
            if (!comment) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "Comment Not Found."
                });
            } else {
                if (super.authorize('delete', 'Comment', req.authUser, comment)) {
                    comment.destroy().then(_ => {
                        return res.status(200).json({status: 'ok', code: 200, message: 'Comment deleted.'});
                    }).catch(_ => {
                        return res.status(500).json({status: 'error', code: 500, message: 'Server error.'});
                    })
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't delete this resource."
                    });
                }
            }
        }).catch(err => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }
}


module.exports = new CommentController();