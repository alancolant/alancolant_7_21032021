const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../Models").User;
const config = require('../../config/global.json');
const Controller = require("./Controller");
const CryptoJS = require("crypto-js");


function encrypt(value) {
    return CryptoJS.AES.encrypt(value, CryptoJS.enc.Utf8.parse(config.randomKey), {
        iv: {
            words: [0, 0, 0, 0],
            sigBytes: 16,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }
    }).toString()
}

class AuthController extends Controller {
    login(req, res, _) {
        User.findOne({
            where: {
                email: encrypt(req.fields.email)
            }
        }).then(user => {
            if (!user) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "User Not Found."
                });
            } else {
                const passwordIsValid = bcrypt.compareSync(req.fields.password, user.password);
                if (!passwordIsValid) {
                    return res.status(400).json(
                        {
                            status: 'error',
                            code: 400,
                            message: "Invalid Password!"
                        }
                    );
                }
                //If succeed authentification
                const token = jwt.sign({
                        id: user.id,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        role: user.role,
                    },
                    config.randomKey,
                    {
                        expiresIn: 86400 // 24 hours
                    });

                return res.status(200).json({status: 'ok', code: 200, data: {token: token, user: user.toJSON()}});
            }
        }).catch(err => {
            console.log(err)
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    signup(req, res, _) {
        console.log(req.fields)
        return User.create(req.fields).then(user => res.json({
            status: 'ok',
            code: 200,
            data: user.toJSON()
        })).catch(err => {
            const formatted_errors = {};
            if (err.errors) {
                err.errors.map(e => {
                    formatted_errors[e.path] = e.message;
                })
            }
            return res.status(400).json({
                status: 'error',
                code: 400,
                errors: formatted_errors
            });
        });
    }
}


module.exports = new AuthController();