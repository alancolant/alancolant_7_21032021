const User = require("../Models").User;
const Controller = require("./Controller");

class UserController extends Controller {
    index(req, res, next) {
        if (super.authorize('list', 'User', req.authUser)) {
            return User.findAndCountAll().then(users => res.json({
                status: 'ok',
                code: 200,
                data: users
            })).catch(err => {
                res.status(500).json({
                    status: 'error',
                    code: 500,
                    message: 'Server error.'
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t list resource.',
                code: 401,
            });
        }
    }

    show(req, res, next) {
        User.findByPk(req.params.id).then(user => {
            if (!user) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "User Not Found."
                });
            } else {
                if (super.authorize('show', 'User', req.authUser, user)) {
                    return res.status(200).json({status: 'ok', code: 200, data: user.toJSON()});
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't view this resource."
                    });
                }
            }
        }).catch(err => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    store(req, res, next) {
        if (super.authorize('create', 'User', req.authUser)) {
            return User.create(req.fields).then(user => res.json({
                status: 'ok',
                code: 200,
                data: user.toJSON()
            })).catch(err => {
                const formatted_errors = {};
                err.errors.map(e => {
                    formatted_errors[e.path] = e.message;
                })
                return res.status(400).json({
                    status: 'error',
                    code: 400,
                    errors: formatted_errors
                });
            });
        } else {
            return res.status(401).json({
                status: 'You can\'t create resource.',
                code: 401,
            });
        }
    }

    update(req, res, next) {
        User.findByPk(req.params.id).then(user => {
            if (!user) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "User Not Found."
                });
            } else {
                if (super.authorize('edit', 'User', req.authUser, user)) {
                    user.update(req.fields).then(user => {
                        return res.status(200).json({status: 'ok', code: 200, data: user.toJSON()});
                    }).catch(err => {
                        const formatted_errors = {};
                        err.errors.map(e => {
                            formatted_errors[e.path] = e.message;
                        })
                        return res.status(400).json({
                            status: 'error',
                            code: 400,
                            errors: formatted_errors
                        });
                    });
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't update this resource."
                    });
                }
            }
        }).catch(err => {
            console.log(err)
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }

    destroy(req, res, next) {
        User.findByPk(req.params.id).then(user => {
            if (!user) {
                return res.status(404).json({
                    status: 'error',
                    code: 404,
                    message: "User Not Found."
                });
            } else {
                if (super.authorize('delete', 'User', req.authUser, user)) {
                    user.destroy().then(_ => {
                        return res.status(200).json({status: 'ok', code: 200, message: 'User deleted.'});
                    }).catch(_ => {
                        return res.status(500).json({status: 'error', code: 500, message: 'Server error.'});
                    })
                } else {
                    return res.status(401).json({
                        status: 'unauthorized',
                        code: 401,
                        message: "You can't delete this resource."
                    });
                }
            }
        }).catch(err => {
            return res.status(500).json({
                status: 'error',
                code: 500,
            });
        });
    }
}


module.exports = new UserController();