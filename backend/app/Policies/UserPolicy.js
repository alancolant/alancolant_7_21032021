const User = require('../Models').User;

class UserPolicy {
    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    list(auth) {
        return auth.role === 'admin';
    }

    /**
     * @param {User} auth Authenticated User
     * @param {User} user Model to search
     * @return boolean
     */
    show(auth, user) {
        return (auth.role === 'admin') || (auth.id === user.id);
    }

    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    create(auth) {
        return auth.role === 'admin';
    }

    /**
     * @param {User} auth Authenticated User
     * @param {User} user Model to search
     * @return boolean
     */
    delete(auth, user) {
        return (auth.role === 'admin') || (auth.id === user.id);
    }

    /**
     * @param {User} auth Authenticated User
     * @param {User} user Model to search
     * @return boolean
     */
    edit(auth, user) {
        return (auth.role === 'admin') || (auth.id === user.id);
    }
}

module.exports = new UserPolicy()