const User = require('../Models').User;
const Report = require('../Models').Report

class ReportPolicy {
    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    list(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Report} report Model to search
     * @return boolean
     */
    show(auth, report) {
        return (auth.role === 'admin') || (auth.id === report.userId);
    }

    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    create(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Report} report Model to search
     * @return boolean
     */
    delete(auth, report) {
        return (auth.role === 'admin') || (auth.id === report.userId);
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Report} report Model to search
     * @return boolean
     */
    edit(auth, report) {
        return (auth.role === 'admin') || (auth.id === report.userId);
    }
}

module.exports = new ReportPolicy()