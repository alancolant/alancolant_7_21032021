const User = require('../Models').User;
const Comment = require('../Models').Comment

class CommentPolicy {
    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    list(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Comment} comment Model to search
     * @return boolean
     */
    show(auth, comment) {
        return (auth.role === 'admin') || (auth.id === comment.userId);
    }

    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    create(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Comment} comment Model to search
     * @return boolean
     */
    delete(auth, comment) {
        return (auth.role === 'admin') || (auth.id === comment.userId);
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Comment} comment Model to search
     * @return boolean
     */
    edit(auth, comment) {
        return (auth.role === 'admin') || (auth.id === comment.userId);
    }
}

module.exports = new CommentPolicy()