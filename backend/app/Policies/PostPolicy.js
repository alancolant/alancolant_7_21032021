const User = require('../Models').User;
const Post = require('../Models').Post;

class PostPolicy {
    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    list(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Post} post Model to search
     * @return boolean
     */
    show(auth, post) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @return boolean
     */
    create(auth) {
        return true;
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Post} post Model to search
     * @return boolean
     */
    delete(auth, post) {
        return (auth.role === 'admin') || (auth.id === post.userId);
    }

    /**
     * @param {User} auth Authenticated User
     * @param {Post} post Model to search
     * @return boolean
     */
    edit(auth, post) {
        return (auth.role === 'admin') || (auth.id === post.userId);
    }
}

module.exports = new PostPolicy()